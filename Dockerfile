FROM node:19-alpine
WORKDIR /app
COPY package.json ./
COPY . ./ 
RUN npm install
RUN npm install ts-node
RUN npm install @orbs-network/ton-access
RUN npm install @ton/ton @ton/crypto @ton/core
RUN npm install dotenv --save
RUN npx ts-node -r dotenv/config getWalletBalance.ts 

#RUN npx ts-node transfer.ts