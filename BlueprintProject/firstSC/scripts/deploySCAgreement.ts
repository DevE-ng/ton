import { toNano } from '@ton/core';
import { SCAgreement } from '../wrappers/SCAgreement';
import { compile, NetworkProvider } from '@ton/blueprint';

export async function run(provider: NetworkProvider) {
    const sCAgreement = provider.open(
        SCAgreement.createFromConfig(
            {
                id: Math.floor(Math.random() * 10000),
                counter: 0,
            },
            await compile('SCAgreement')
        )
    );

    await sCAgreement.sendDeploy(provider.sender(), toNano('0.001'));

    await provider.waitForDeploy(sCAgreement.address);

    console.log('ID', await sCAgreement.getID());
}
